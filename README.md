This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## What Is it?

this is very simple React validation component. for now, it implemented using Snapp-ui-kit so you just need to add snapp-ui-kit to see demo. but it works with any Input and UI kit. 
### Basic Usage

just run the project and change Validation props on General Input Component. you can use default validation rules that I have written for this project or use your custom pattern for it. 

## ROAD MAP

- Add typescript
- Better file and component structure
- Add seperate validation component (seperat it from "Form" component that already developed)
- more default validation rules
- default error message support



## notes to mentions

this is just an MVP with couple of hours of development and has not been completed yet. it's will be going to be completed in near future.
```javascript
<GeneralInput
	label='min length'
	placeholder='Please enter your name (not less than 3 chars)'
	onChange={(e) => setValue(e.target.value)}
	value={value}
	name='inputOne'
	validation={{
		[validationTypes.minLength]: { // IT COMES FRO VALIDATION TYPS UTILS
			parameter: 3,
			errorMessage: <span> shouldn't be less than 3</span>
		}
	}}
/>
```
				
