import React, { useState, useEffect } from "react";
import validatorFunctions from "./Validation";

function Form({ children, onFormChange, onValidateStateChange }) {
	const createFormFiels = () => {
		return children.reduce((previousValue, currentValue) => {
			const item = currentValue.props;
			previousValue[item.name] = {
				value: item.value,
				validation: item.validation || null
			};
			return previousValue;
		}, {});
	};
	const formFields = createFormFiels();

	const getFiledsWithValidation = () => {
		return Object.keys(formFields).reduce((previousValue, currentValue) => {
			if (formFields[currentValue].validation) {
				previousValue[currentValue] = formFields[currentValue];
			}
			return previousValue;
		}, {});
	};

	const [formValues, setFormValues] = useState(formFields);
	const [formErrors, setFormErrors] = useState({});

	const handleOnChange = (e, name, defaultInputOnChange, validation) => {
		const value = e.target.value;
		defaultInputOnChange(e);
		setFormValues({
			[name]: value
		});

		if (validation) {
			const validationArray = Object.keys(validation);
			for (let i = 0; i < validationArray.length; i++) {
				if (
					checkValidation(
						validationArray[i],
						value,
						validation[validationArray[i]].parameter
					)
				) {
					setFormErrors({
						...formErrors,
						[name]: null
					});
				} else {
					setFormErrors({
						...formErrors,
						[name]: validation[validationArray[i]].errorMessage
					});
				}
			}
		}
	};

	const checkValidation = (validatorItem, value, parameter) => {
		if (!validatorFunctions[validatorItem](value, parameter)) {
			return false;
		} else {
			return true;
		}
	};

	useEffect(() => {
		onValidateStateChange(formErrors);
	}, [formErrors]);
	useEffect(() => {
		onFormChange(formValues);
	}, [formValues]);
	return (
		<>
			{children.map((child) => {
				return React.cloneElement(child, {
					onChange: (e) => {
						handleOnChange(
							e,
							child.props.name,
							child.props.onChange,
							child.props.validation || null
						);
					},
					error: formErrors[child.props.name] ? true : false,
					helperText: formErrors[child.props.name]
						? formErrors[child.props.name]
						: ""
				});
			})}
		</>
	);
}

export default Form;
