import React, { useState } from "react";
import { ThemeProvider, GeneralInput } from "@snapp/ui-kit";

import "./App.css";

import Form from "./Form";

const validationTypes = {
	isRequired: "isRequired",
	minLength: "minLength",
	maxLength: "maxLength",
	numberOnly: "numberOnly",
	phoneNumber: "phoneNumber",
	email: "email",
	customPattern: "customPattern"
};

function App() {
	const [value, setValue] = useState("");
	const [secondValue, setSecondValue] = useState("");
	const [valueThree, setValueThree] = useState("");
	const [valueFour, setValueFour] = useState("");
	const [valueFive, setValueFive] = useState("");

	return (
		<div className='App'>
			<ThemeProvider cssHelpers>
				<Form
					onChange={(values) => values}
					onFormChange={(formValues) => formValues}
					onValidateStateChange={(isValid) => isValid}>
					<GeneralInput
						label='min length'
						placeholder='Please enter your name (not less than 3 chars)'
						onChange={(e) => setValue(e.target.value)}
						value={value}
						name='inputOne'
						validation={{
							[validationTypes.minLength]: {
								parameter: 3,
								errorMessage: <span> shouldn't be less than 3</span>
							}
						}}
					/>
					<GeneralInput
						label='max length'
						error={true}
						placeholder='Please enter your name (not more than 6 chars)'
						onChange={(e) => setSecondValue(e.target.value)}
						value={secondValue}
						name='inputTwo'
						validation={{
							[validationTypes.maxLength]: {
								parameter: 6,
								errorMessage: <span> shouldn't be more than 6</span>
							}
						}}
					/>
					<GeneralInput
						label='email'
						error={true}
						placeholder='Please enter your email'
						onChange={(e) => setValueThree(e.target.value)}
						value={valueThree}
						name='inputThree'
						validation={{
							[validationTypes.email]: {
								errorMessage: <span> should be valid email</span>
							}
						}}
					/>
					<GeneralInput
						label='custom pattern'
						error={true}
						placeholder='Please enter your custom data'
						onChange={(e) => setValueFour(e.target.value)}
						value={valueFour}
						name='inputFour'
						validation={{
							[validationTypes.customPattern]: {
								parameter: /^[0-9]+$/,
								errorMessage: <span> custom pattern please</span>
							}
						}}
					/>
					<GeneralInput
						label='without any validation '
						error={true}
						placeholder='Please enter your name'
						onChange={(e) => setValueFive(e.target.value)}
						value={valueFive}
						name='inputFive'
					/>
				</Form>
			</ThemeProvider>
		</div>
	);
}

export default App;
