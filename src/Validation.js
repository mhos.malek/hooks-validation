const onlyNumberValidationPattern = /^[0-9]+$/;
const validatorFunctions = {
	maxLength: (value, parameter) => {
		if (value.length > parameter) {
			return false;
		}
		return true;
	},
	minLength: (value, parameter) => {
		if (value.length < parameter) {
			return false;
		}
		return true;
	},
	numberOnly: (value, parameter = null) => {
		if (!onlyNumberValidationPattern.test(value)) {
			return false;
		}
		return true;
	},
	isRequired: (value, parameter = null) => {
		if (!value && value === "") {
			return false;
		}
		return true;
	},
	email: (value, parameter = null) => {
		if (!/^\S+@\S+\.\S+$/.test(value)) {
			return false;
		}
		return true;
	},
	customPattern: (value, pattern) => {
		if (!pattern.test(value)) {
			return false;
		}
		return true;
	}
};

export default validatorFunctions;
